package com.example.manuelbravo.lab3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity{

   GameView gameView;

    Sensor sensor;
    SensorManager sensorManager;


    private ToneGenerator toneGenerator;
    private Vibrator vibrator;


    private SensorListener listener;

    AlertListener alertUpdate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Sensors and listener.
        this.sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.listener = new SensorListener();

        //Vibrator and tone. Listener that activate them
        this.toneGenerator = new ToneGenerator(AudioManager.STREAM_SYSTEM, ToneGenerator.MAX_VOLUME);
        this.vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        this.alertUpdate=new AlertListener();
        registerReceiver(this.alertUpdate, new IntentFilter("GenerateNewAlert"));


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //New View that contains the GameView.
        gameView =new GameView(this);
        setContentView(gameView);
    }


    /**
     * unregisters listener to stop reacting on sensor data
     */
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this.listener);
    }



    /**
     * registers listener to resume reacting on sensor data
     */
    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this.listener, this.sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onDestroy(){
       super.onDestroy();
       sensorManager.unregisterListener(this.listener);



    }

    /**
     * The phone will vibrate and generate a sound alert
     *
     */
    public void generateAlert() {
        vibrator.vibrate(100);
        toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP, 100);
    }

    /**
     * initialize sensor manager and rotation sensor
     */
    private class SensorListener implements SensorEventListener {

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            gameView.updateCircleMovement(sensorEvent.values[1], sensorEvent.values[0]);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    }

    /**
     * When a bouncing is listened it will generate and alert.
     *
     */
    public class AlertListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            generateAlert();

        }
    }

}
