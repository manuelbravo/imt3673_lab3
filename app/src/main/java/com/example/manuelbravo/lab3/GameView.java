package com.example.manuelbravo.lab3;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;;
import android.util.Log;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class GameView extends View {

    Context context;

    private Paint rectangle;
    private Paint circle;

    private float circleX;
    private float circleY;

    private float circleSpeedX;
    private float circleSpeedY;

    private float circleAccelX;
    private float circleAccelY;

    private float circleWidth;


    public GameView(Context context) {

        super(context);

        this.context=context;
        this.rectangle = new Paint();
        this.circle = new Paint();
        this.circle.setColor(Color.RED);

    }

    /**
     *  Called when the size of this view has changed
     *  When size changes -> reposition ball to middle and set size appropriately
     * @param currentWidth
     * @param currentHeight
     * @param oldWidth
     * @param oldHeight
     */
    @Override
    protected void onSizeChanged(int currentWidth, int currentHeight, int oldWidth, int oldHeight) {
        super.onSizeChanged(currentWidth, currentHeight, oldWidth, oldHeight);

        //The circle will be positioned in the middle of the screen.
        //The circle's width will be 5% of the the width of the screen.
        this.circleX = currentWidth/2;
        this.circleY = currentHeight/2;
        this.circleWidth = (float) (currentWidth * 0.05);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        // Update the position of the circle
        this.circleX = this.circleX + circleSpeedX;
        this.circleY = this.circleY + circleSpeedY;

        // Update the speed of the circle.
        this.circleSpeedX = this.circleSpeedX + circleAccelX;
        this.circleSpeedY = this.circleSpeedY + circleAccelY;

        // Reduce the speed
        this.circleSpeedX = (float) (this.circleSpeedX * 0.98);
        this.circleSpeedY = (float) (this.circleSpeedY * 0.98);



        //When the circle hits the boundaries of the rectangle
        //the phone will vibrate and emit a sound.
       if (this.circleX < this.circleWidth +20) {

                generateAlert();
                this.circleSpeedX *= -1; //
                this.circleX = this.circleWidth +20+1;
           Log.d("Bouncing","BOUNCING LEFT: View WIDTH: "+ getWidth() + " Circle Width: " + this.circleX + "  View height: "+ getHeight()+ " Circle Height: "+this.circleY );


        }

        if (this.circleX > getWidth() - this.circleWidth -20) {

            generateAlert();
            this.circleSpeedX *= -1;
            this.circleX = getWidth() - this.circleWidth - 20 - 1;
            Log.d("Bouncing","BOUNCING RIGHT: View WIDTH: "+ getWidth() + " Circle Width: " + this.circleX + "  View height: "+ getHeight()+ " Circle Height: "+this.circleY );
        }

        if (this.circleY < this.circleWidth +20) {

            generateAlert();
            this.circleSpeedY *= -1;
            this.circleY = this.circleWidth +20 + 1;
            Log.d("Bouncing","BOUNCING UP: View WIDTH: "+ getWidth() + " Circle Width: " + this.circleX + "  View height: "+ getHeight()+ " Circle Height: "+this.circleY );
        }

       if (this.circleY > getHeight() - this.circleWidth -20) {

            generateAlert();
            this.circleSpeedY *= -1;
            this.circleY = getHeight() - this.circleWidth -20 - 1;
           Log.d("Bouncing","BOUNCING DOWN: View WIDTH: "+ getWidth() + " Circle Width: " + this.circleX + "  View height: "+ getHeight()+ " Circle Height: "+this.circleY );

        }

        //Draw the rectangle and the circle
        canvas.drawRect(new Rect(20, 20, getWidth()-20, getHeight()-20), rectangle);
        canvas.drawCircle(circleX, circleY, circleWidth, circle);


        postInvalidate();

    }


    public void updateCircleMovement(float newAccX, float newAccY){

        circleAccelX = (float) 0.2  * newAccX;
        circleAccelY = (float) 0.2 * newAccY;
    }




    public void generateAlert() {
        Intent intent = new Intent("GenerateNewAlert");
        context.sendBroadcast(intent);
    }



}